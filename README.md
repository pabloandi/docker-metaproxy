# README #

Indexdata's Metaproxy is a Z39.50 search and extract protocol proxy server. It can provide a single searching connection from one or more Z39.50 servers.

### Docker ###

* From: ubuntu:trusty
* ADD: apt-key from indexdata, indexdata.asc
* COPY: example of a configuration file metaproxy.xml. Feel free to change it
* EXPOSE: port 9000

### Requirements ###

Free IP and ports for every Z39.50 server connection

### Who do I talk to? ###

* pabloandi@gmail.com