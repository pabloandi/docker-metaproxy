FROM ubuntu:trusty
MAINTAINER Pablo Andrés Díaz <pabloandi@gmail.com>

ADD indexdata.asc indexdata.asc

RUN echo "deb http://ftp.indexdata.dk/ubuntu trusty main" >> /etc/apt/sources.list && echo "deb-src http://ftp.indexdata.dk/ubuntu trusty main" >> /etc/apt/sources.list
RUN apt-key add indexdata.asc
RUN apt-get update && apt-get -y install metaproxy

COPY metaproxy.xml /etc/metaproxy/metaproxy.xml

EXPOSE 9000

CMD ["/usr/bin/metaproxy", "-c/etc/metaproxy/metaproxy.xml"]
